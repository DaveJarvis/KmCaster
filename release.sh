#!/usr/bin/env bash

RELEASE=$(git describe --abbrev=0 --tags)

cat tokens/release.pat | glab auth login --hostname gitlab.com --stdin

glab release upload ${RELEASE} build/libs/kmcaster.jar

